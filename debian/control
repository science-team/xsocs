Source: xsocs
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Picca Frédéric-Emmanuel <picca@synchrotron-soleil.fr>,
           Alexandre Marie <alexandre.marie@synchrotron-soleil.fr>
Section: science
Priority: optional
Build-Depends: debhelper-compat(= 12),
               dh-python,
               python3,
               python3-fabio,
               python3-h5py,
               python3-matplotlib (>= 1.4.2),
               python3-numpy,
               python3-opengl (>= 3.0.2),
               python3-pyqt5 (>= 5.3.2),
               python3-scipy (>= 0.14.0),
               python3-setuptools,
               python3-silx (>= 0.9.0),
               python3-sphinx <!nodoc>,
               python3-xrayutilities
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/science-team/xsocs
Vcs-Git: https://salsa.debian.org/science-team/xsocs.git
Homepage: https://gitlab.esrf.fr/kmap/xsocs/

Package: xsocs
Architecture: any
Section: python
Depends: python3-opengl,
         python3-pyqt5,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: xsocs-doc
Description: Automatic analysis of 5D sets of data (Python 3)
 The X-ray Strain Orientation Calculation Software (X-SOCS) is a
 user-friendly software, developed for automatic analysis of 5D sets
 of data recorded during continuous mapping measurements. X-SOCS aims
 at retrieving strain and tilt maps of nanostructures, films, surfaces
 or even embedded structures.
 .
 This is the Python3 version of the package.

Package: xsocs-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Automatic analysis of 5D sets of data (documentation)
 The X-ray Strain Orientation Calculation Software (X-SOCS) is a
 user-friendly software, developed for automatic analysis of 5D sets
 of data recorded during continuous mapping measurements. X-SOCS aims
 at retrieving strain and tilt maps of nanostructures, films, surfaces
 or even embedded structures.
 .
 This package includes the manual in HTML format.
