Trouble shooting
================

Using XSocs through ssh
-----------------------

XSocs is using OpenGL 2.1 widgets from the `silx <http://www.silx.org/doc/silx/latest/>`_ toolkit for QSpace visualization.
When running through ssh, there are a few reasons that can avoid the display of the 3D QSpace visualization.

See `Using OpenGL through ssh <http://www.silx.org/doc/silx/latest/troubleshooting.html#using-opengl-through-ssh>`_ in `silx <http://www.silx.org/doc/silx/latest/>`_ documentation for more information.

You can disable the 3D view of the QSpace with the ``--no-3d`` option from the command line.
