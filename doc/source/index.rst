
X-SOCS's documentation
======================

The X-ray Strain Orientation Calculation Software (X-SOCS) is
a user-friendly software, developed for automatic analysis of
5D sets of data recorded during continuous mapping measurements.
X-SOCS aims at retrieving strain and tilt maps of nanostructures,
films, surfaces or even embedded structures.

.. toctree::
    :hidden:

    install.rst
    using.rst
    tutorials.rst
    modules.rst
    fileformat.rst
    troubleshooting.rst

:doc:`install`
    How to install *X-SOCS*

:doc:`using`
    How to use *X-SOCS* to reduce KMAP data

:doc:`tutorials`
   Video tutorials

:doc:`modules`
   Documentation of the script API

:doc:`fileformat`
   Description of the input HDF5 file format

:doc:`troubleshooting`
   When things do not work as expected

Resources:

- `Source code repository <https://gitlab.esrf.fr/kmap/xsocs>`_
- `Issue tracker <https://gitlab.esrf.fr/kmap/xsocs/issues>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

